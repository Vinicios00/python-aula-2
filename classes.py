#class CalculoImposto(object):
#    def __init__(self):
#        self._taxa = 0

#    @property
#    def taxa(self):
#        return round(self._taxa, 2)

#    @taxa.setter
#    def taxa(self, val_taxa):
#        if val_taxa < 0:
#            self._taxa = 0
#        elif val_taxa >100:
#          self._taxa = 100
#        else:
#           self._taxa = val_taxa

class ControlarDesconto(object):
    def __init__(self, preco_produto, porcentagem_desconto):
        self.preco_produto = preco_produto
        self.__porcentagem_desconto = porcentagem_desconto

    def __porcentagem_desconto(self, porcentagem):
        if isinstance(porcentagem, int) and 1 <= porcentagem <= 35:
            return porcentagem
        else:
            print("Porcentagem de desconto inválida. Utilizando 1 como valor padrão.")
            return 1

    @property
    def porcentagem_desconto(self):
        return self.__porcentagem_desconto

    @porcentagem_desconto.setter
    def porcentagem_desconto(self, value):
        self.__porcentagem_desconto = self.__validate_porcentagem(value)

    def calcular_desconto(self):
        desconto_aplicado = self.preco_produto * (self.__porcentagem_desconto / 100)
        preco_com_desconto = self.preco_produto - desconto_aplicado
        return desconto_aplicado, preco_com_desconto