class ContaBanco:
    def __init__(self, num_conta, nome, saldo):
        self.num_conta = num_conta
        self.nome = nome
        self.saldo = saldo

    def depositar(self, valor):
        self.saldo += valor
        return self.saldo

    def sacar(self, valor):
        self.saldo -= valor
        return self.saldo

    def alterar_nome(self, nome):
        self.nome = nome
        return nome

conta = ContaBanco(234535, "Nome", 0)

while True:
    opcao = input("Digite 1 para Depositar, 2 para Sacar, 3 para Alterar seu nome, ou Q para sair: ")

    if opcao == '1':
        saldo = float(input("Digite o valor que deseja depositar: "))
        conta.depositar(saldo)
        print("Novo saldo:", conta.saldo)

    elif opcao == '2':
        saldo = float(input("Digite o valor que deseja sacar: "))
        conta.sacar(saldo)
        print("Novo saldo:", conta.saldo)

    elif opcao == '3':
        novo_nome = input("Digite o novo nome: ")
        conta.alterar_nome(novo_nome)
        print("Novo nome:", conta.nome)

    elif opcao == 'q':
        print("Saindo do programa...")
        break

    else:
        print("Opção inválida, tente novamente.")