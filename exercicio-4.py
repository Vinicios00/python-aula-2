from classes import ControlarDesconto

preco = float(input("Digite o preço do produto: "))
porcentagem = int(input("Digite a porcentagem de desconto: "))

produto = ControlarDesconto(preco, porcentagem)

desconto, preco_final = produto.calcular_desconto()
print(f"Porcentagem de desconto aplicada: {produto.porcentagem_desconto}%")
print(f"Valor do produto com desconto aplicado: R${preco_final}")

